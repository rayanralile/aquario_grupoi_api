﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AquarioGrupoIAPI.Data;
using AquarioGrupoIAPI.Models;

namespace AquarioGrupoIAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DadosController : ControllerBase
    {
        private readonly AquarioGrupoIAPIContext _context;

        public DadosController(AquarioGrupoIAPIContext context)
        {
            _context = context;
        }

        // GET: api/Dados
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Dados>>> GetDados()
        {
          if (_context.Dados == null)
          {
              return NotFound();
          }
            return await _context.Dados.ToListAsync();
        }

        // GET: api/Dados/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Dados>> GetDados(int id)
        {
          if (_context.Dados == null)
          {
              return NotFound();
          }
            var dados = await _context.Dados.FindAsync(id);

            if (dados == null)
            {
                return NotFound();
            }

            return dados;
        }

        // PUT: api/Dados/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDados(int id, Dados dados)
        {
            if (id != dados.Id)
            {
                return BadRequest();
            }

            _context.Entry(dados).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DadosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dados
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Dados>> PostDados(Dados dados)
        {
          if (_context.Dados == null)
          {
              return Problem("Entity set 'AquarioGrupoIAPIContext.Dados'  is null.");
          }
            _context.Dados.Add(dados);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDados", new { id = dados.Id }, dados);
        }

        // DELETE: api/Dados/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDados(int id)
        {
            if (_context.Dados == null)
            {
                return NotFound();
            }
            var dados = await _context.Dados.FindAsync(id);
            if (dados == null)
            {
                return NotFound();
            }

            _context.Dados.Remove(dados);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool DadosExists(int id)
        {
            return (_context.Dados?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
