﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AquarioGrupoIAPI.Models;

namespace AquarioGrupoIAPI.Data
{
    public class AquarioGrupoIAPIContext : DbContext
    {
        public AquarioGrupoIAPIContext (DbContextOptions<AquarioGrupoIAPIContext> options)
            : base(options)
        {
        }

        public DbSet<AquarioGrupoIAPI.Models.Dados>? Dados { get; set; }
    }
}
